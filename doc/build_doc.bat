@ECHO off
REM ###############################################################################
REM # @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
REM # 	All Rights Reserved.
REM # 	Unauthorized copying of this file, via any medium is strictly prohibited
REM # 	Dissemination of this information or reproduction of this material
REM # 	is strictly forbidden unless prior written permission is obtained
REM # 	from Guichet Entreprises.
REM ###############################################################################
ECHO ------------------------------------------
:BEGIN
SET DOXYGEN_PATH=C:\\Program Files\\doxygen\\bin
SET DOXYGEN_EXE=doxygen.exe
SET DOXYGEN_CMD=%DOXYGEN_PATH%\\%DOXYGEN_EXE%

SET CONFIG_FILE=%~dp0\\config_doc.dox

IF EXIST "%DOXYGEN_CMD%" (
	ECHO "Found doxygen %DOXYGEN_CMD%"
) ELSE (
	ECHO "%DOXYGEN_CMD%"
	ECHO "Doxygen not found"
	GOTO:END
)

IF EXIST "%CONFIG_FILE%" (
	ECHO "Found config file %CONFIG_FILE%"
) ELSE (
	ECHO "%CONFIG_FILE%"
	ECHO "Config file not found"
	GOTO:END
)

ECHO "Start"
"%DOXYGEN_CMD%"  "%CONFIG_FILE%"
:END
ECHO ------------------------------------------
