﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

import logging
import argparse
import imaplib
import email

from cfe.services import commons

###############################################################################
# Define the parsing of arguments of the command line
###############################################################################
def get_parser_for_command_line():
    description_arg = """."""

    parser = argparse.ArgumentParser(description=description_arg)
    parser.add_argument('--verbose', '-v',
                        action='store_true', dest='verbose',
                        help='Put the logging system on the console for info.')
    parser.add_argument('--login', '-l', help='email login')
    parser.add_argument('--password', '-p', help='email password')
    parser.add_argument('--host', '-H', help='email host')
    parser.add_argument('--port', '-P', help='email port')
    parser.add_argument('--dir', '-d', help='email directory')
    parser.add_argument('--subject', '-s', help='subject filter (in)')
    parser.add_argument('--sender', '-S', help='sender email filter')
    parser.add_argument('--recipient', '-r', help='recipient email filter')

    return parser.parse_args()

###############################################################################
# Main script
###############################################################################
def main():

    logging.info('-------------------------------------------------------->>')
    logging.info('cfe-tools')
    logging.info('-------------------------------------------------------->>')
    args = get_parser_for_command_line()

    email_user = args.login
    email_pass = args.password
    host = args.host
    port = args.port

    mail = imaplib.IMAP4_SSL(host, port)
    mail.login(email_user, email_pass)

    files = get_list_file(mail, directory=args.dir,
                          sender=args.sender,
                          subject=args.subject,
                          recipient=args.recipient)

    print(len(files))
    # for file in files:
    #     for line in file.splitlines():
    #         print(line)

    logging.info('-------------------------------------------------------->>')
    logging.info('Finished')
    logging.info('<<--------------------------------------------------------')

def get_list_file(mail, sender=None, recipient=None, subject=None,
                  directory="Inbox/Retour_cfe_ne_pas_toucher"):

    # recherche des mails
    mail.select(directory)
    _, email_list = mail.search(None, 'ALL')
    id_list = email_list[0].split()

    files = []

    for num in id_list:
        # chargement du mail
        _, data = mail.fetch(num, '(RFC822)')

        # convertion en String
        raw_email_string = data[0][1].decode('utf-8')
        email_message = email.message_from_string(raw_email_string)

        if(subject and not subject in email_message['subject']
           or recipient and not recipient in email_message['to']
           or sender and not sender in email_message['from']):
            continue

        # chargement des PJ
        for message in email_message.walk():
            if message.get_content_maintype() == 'multipart':
                continue
            if message.get('Content-Disposition') is None:
                continue
            file_name = message.get_filename()

            if file_name.endswith(".csv"):
                file = message.get_payload(None, True)
                files.append(file.decode())
    return files


###############################################################################
# Call main if the script is main
###############################################################################
if __name__ == '__main__':
    commons.set_logging_system()
    main()
