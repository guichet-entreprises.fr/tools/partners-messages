﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
###############################################################################
# @copyright Copyright (C) Guichet Entreprises - All Rights Reserved
# 	All Rights Reserved.
# 	Unauthorized copying of this file, via any medium is strictly prohibited
# 	Dissemination of this information or reproduction of this material
# 	is strictly forbidden unless prior written permission is obtained
# 	from Guichet Entreprises.
###############################################################################

import sys
import codecs
import logging
import os.path
import tempfile

RESSOURCES_PATH = "ressources"

def is_frozen():
    return getattr(sys, 'frozen', False)

###############################################################################
# Find the filename of this file (depend on the frozen or not)
###############################################################################
def get_this_filename():
    result = ""

    if getattr(sys, 'frozen', False):
        # frozen
        result = sys.executable
    else:
        # unfrozen
        result = __file__

    return result

###############################################################################
# Test a folder
# Test if the folder exist.
#
# @exception RuntimeError if the name is a file or not a folder
#
# @param folder the folder name
# @return the folder normalized.
###############################################################################
def check_folder(folder):
    if os.path.isfile(folder):
        logging.error('%s can not be a folder (it is a file)', folder)
        raise RuntimeError('%s can not be a folder (it is a file)' % folder)

    if not os.path.isdir(folder):
        logging.error('%s is not a folder', folder)
        raise RuntimeError('%s is not a folder' % folder)

    return set_correct_path(folder)

###############################################################################
# Retrive the correct complet path
###############################################################################
def set_correct_path(the_path):
    return os.path.abspath(the_path)

###############################################################################
# test if this is a file and correct the path
###############################################################################
def test_is_file_and_correct_path(filename):
    filename = set_correct_path(filename)

    if not os.path.isfile(filename):
        logging.error('"%s" is not a file', filename)
        raise Exception('"%s" is not a file' % (filename))

    return filename

###############################################################################
# test if this is a file and correct the path
###############################################################################
def test_is_directory(directory):
    directory = set_correct_path(directory)

    if not os.path.isdir(directory):
        logging.error('"%s" is not a directory', (directory))
        raise Exception('"%s" is not a directory' % (directory))

    return directory

###############################################################################
# Test a folder
###############################################################################
def create_path(path):
    paths_to_create = []
    while not os.path.lexists(path):
        paths_to_create.insert(0, path)
        head, tail = os.path.split(path)
        if len(tail.strip()) == 0:  # Just incase path ends with a / or /
            path = head
            head, tail = os.path.split(path)
        path = head

    for path in paths_to_create:
        os.mkdir(path)


###############################################################################
# Logging system
###############################################################################
def set_logging_system(level=logging.INFO):
    log_filename = os.path.abspath(os.path.join(os.path.split(
        os.path.realpath(get_this_filename()))[0], '../../', 'nash-tools.log'))

    if is_frozen():
        log_filename = os.path.abspath(os.path.join(
            tempfile.gettempdir(), 'nash-tools.log'))

    logging.basicConfig(filename=log_filename, level=logging.INFO,
                        format='%(asctime)s: %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')
    console = logging.StreamHandler()
    console.setLevel(level)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(asctime)s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

###############################################################################
# Get the content of a file. This function delete the BOM.
#
# @param filename the file name
# @param encoding the encoding of the file
# @return the content
###############################################################################
def get_file_content(filename, encoding="utf-8"):
    logging.debug('Get content of the filename %s', filename)
    filename = test_is_file_and_correct_path(filename)

    try:
        input_file = codecs.open(filename, mode="r", encoding=encoding)
        type_bytes = False
        content = input_file.read()
    except UnicodeDecodeError:
        input_file = codecs.open(filename, mode="rb")
        type_bytes = True
        content = input_file.read()

    input_file.close()

    if not type_bytes and content.startswith(u'\ufeff'):
        content = content[1:]

    return content

###############################################################################
# Get boolean according to the string
#
# @param value the string to test
# @return the boolean value
###############################################################################
def str2bool(value):
    return value.lower() in ("yes", "true", "t", "1")


# def prettify_xml(xml_string):
#     my_xml = etree.XML(xml_string.encode('utf-8'))
#     return etree.tostring(my_xml, pretty_print=True).decode("utf-8")
